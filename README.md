# CompleteMe #

A basic Android application that shows a list of words which complete whatever
the user is typing (AutoComplete).

![a1.png](https://bitbucket.org/repo/ozgnnb/images/573882465-a1.png) ![a2.png](https://bitbucket.org/repo/ozgnnb/images/3367305210-a2.png) ![b1.png](https://bitbucket.org/repo/ozgnnb/images/2141658522-b1.png) ![b2.png](https://bitbucket.org/repo/ozgnnb/images/1420621466-b2.png)

## Description ##
The application will return autocomplete results based on user's input.
The results are case-sensitive and filtered alphabetically (using a ternary search tree).

**The application has two lunch modes:**

**Standard (application drawer)** - In this mode, the application will let the user type
an input and display matching words in real-time.

**From another application (share text)** - In this mode, the application will receive an input from another application
 as a text and will display autocomplete results for that word. Selecting a word from the list will return it as a result
 to the original application.
 Typing free text is not allowed in this mode.

 * The application supports large data sources(limited by SQLite), initial loading of the words from the data source
 might take a while, the user can work with the application (make lookups) in the meantime however the word list is incomplete.

* The application does a periodic data source check and will update it's own list accordingly,
 the screen will auto-refresh with the new results.

 * In the current implementation, words removed from the data store will not be removed from the application.
 New words will be added as expected.

## Prerequisites ##

* Android Studio 1.2.*
* Junit 4.12
* Gradle 2.2.1
* Android build tools 22.0.1

## Installation ##

* Pull
* Build
* put CompleteMeDic.txt on the SD card.
* Run / share text from another application.

** Note: Minimal API level is 15 **


## Usage ##

### Standard mode ###
* Type in minimum of 2 characters to start showing results.
* Picking any of the words in this mode is reserved for future development.

### From another application ###
* Start provided WordShare application to type in a term to autocomplete.
Click Share and select the "CompleteMe" application.
* Selecting a word in CompleteMe will return it to WordShare and will display
the selected word.

* Any other text sharing app can be used to lunch CompleteMe however it might not support handling the selected word.

### General ###
* Change the content of the data source and the application will update the current search results accordingly.
* In the current implementation, if the data source (file) is not found the periodic check will cease.

## Testing ##

The project is bundled with a unit test class called TSTSearchEngineTest.class that is designed to test the sorting
logic (TSTSearchEngine) behind the application.

The unit test consists of 9 sub test as follows:

* Null as an input
* Empty list as an input
* Alphabetically sorted list
* Alphabetically reversed sorted list
* Duplicate input
* Capital letters as input
* Caps as input
* Empty string as one of the values in the list
* Mix of letters/order/caps

![AS_test.png](https://bitbucket.org/repo/ozgnnb/images/1734908996-AS_test.png)

To run the test:

* Open the project as usual
* Open the Build Variants tab
* Set Test Artifact to "Unit Tests"
* Right click TSTSearchEngineTest under app\src\test\java and select "Run" and TSTSearchEngineTest with the green Gradle icon.
* Results will be displayed in the Run tab.


## Technologies and third part libraries ##

* [Android studio](https://developer.android.com/sdk) as IDE
* [BitBucket](https://bitbucket.org) as VCS
* [Genymotion](https://www.genymotion.com/) as test device
* [JUnit](http://junit.org/) Integrated with Android studio
* [ButterKnife](jakewharton.github.io/butterknife) by Jake Wharton
* [Timber](https://github.com/JakeWharton/timber) by Jake Wharton
* [Bolts](https://github.com/BoltsFramework/Bolts-Android)
* [JStock](https://github.com/yccheok/jstock) - Sorting ternary search tree only.

## License ##
Copyright 2015 Mark at [1492Mark@gmail.com](1492Mark@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.