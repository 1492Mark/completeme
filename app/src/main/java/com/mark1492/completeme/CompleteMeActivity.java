package com.mark1492.completeme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;


public class CompleteMeActivity extends AppCompatActivity implements CompleteMeFragment.OnDataReceived {
    public static final String PASSED_WORD_KEY = "com.1492mark.PASSED_WORD_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_me);

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            CompleteMeFragment completeMeFragment = new CompleteMeFragment();
            completeMeFragment.setArguments(handleTextShareIntent(getIntent()));
            fragmentTransaction.add(R.id.fragment_container, completeMeFragment);
            fragmentTransaction.commit();
        }
    }

    private Bundle handleTextShareIntent(Intent intent) {
        Bundle bundle = null;

        String action = intent.getAction();
        String type = intent.getType();
        String textExtra = intent.getStringExtra(Intent.EXTRA_TEXT);

        if (action != null && action.equals(Intent.ACTION_SEND)) {
            if (type != null && type.equals("text/plain")) {
                if (textExtra != null && textExtra.length() >= CompleteMeFragment.SEARCH_LENGTH_THRESHOLD) {
                    bundle = new Bundle();
                    bundle.putString(PASSED_WORD_KEY, textExtra.trim());
                } else {
                    returnActivityResult(RESULT_CANCELED, getString(R.string.invalid_data_pass));
                }
            }
        }

        return bundle;
    }

    private void returnActivityResult(int resultCode, String selectedWord) {
        Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_TEXT, selectedWord);
        this.setResult(resultCode, intent);
        finish();
    }

    @Override
    public void onDataReceived(String selectedWord) {
        returnActivityResult(RESULT_OK, selectedWord);
    }
}
