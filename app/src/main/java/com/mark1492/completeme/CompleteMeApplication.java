package com.mark1492.completeme;

import android.app.Application;

import timber.log.Timber;

public class CompleteMeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
