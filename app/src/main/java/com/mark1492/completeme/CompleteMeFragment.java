package com.mark1492.completeme;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import bolts.Continuation;
import bolts.Task;
import butterknife.ButterKnife;
import butterknife.InjectView;
import timber.log.Timber;

public class CompleteMeFragment extends Fragment {
    public static final int SEARCH_LENGTH_THRESHOLD = 2;
    @SuppressWarnings("FieldCanBeLocal") private final int PERIODIC_START_DELAY = 0;
    @SuppressWarnings("FieldCanBeLocal") private final int PERIODIC_RATE = 30;

    @InjectView(R.id.fragment_complete_me_title) TextView titleTextView;
    @InjectView(R.id.fragment_complete_me_recycler) RecyclerView listRecyclerView;
    @InjectView(R.id.fragment_complete_me_input) EditText inputEditText;
    @InjectView(R.id.swipe_refresh_main_view) SwipeRefreshLayout swipeRefreshLayout;

    private ArrayList<String> filteredList = new ArrayList<>();
    private TimerTask timerTask;
    private Timer timer;
    private OnDataReceived dataPasser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_complete_me, container, false);
        ButterKnife.inject(this, view);

        initView();

        startTimer();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnDataReceived) {
            dataPasser = (OnDataReceived) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        dataPasser = null;
    }

    private void initView() {
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);

        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setProgressViewOffset(true, 150, 500);

        if (startedForResult()) {
            inputEditText.setVisibility(View.GONE);
        } else {
            inputEditText.addTextChangedListener(new InputWatcher());
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        listRecyclerView.setLayoutManager(layoutManager);
        listRecyclerView.setAdapter(new WordsAdapter(filteredList, new ItemClickedListener() {

            @Override
            public void onClickItem(String word) {
                Timber.e("Clicked: %s", word);
                if (startedForResult()) {
                    if (dataPasser != null) {
                        dataPasser.onDataReceived(word);
                    }
                }
            }
        }));

        updateTitle();
    }

    private void getFilteredList(String beginsWith) {
        if (beginsWith.length() < SEARCH_LENGTH_THRESHOLD) {
            filteredList.clear();
            updateTitle();
        } else {
            DataAccessor dataAccessor = DataAccessor.getInstance(getActivity());
            dataAccessor.getFilteredList(beginsWith).continueWith(
                    new Continuation<ArrayList<String>, Void>() {

                        @Override
                        public Void then(Task<ArrayList<String>> task) throws Exception {
                            if (task.isFaulted()) {
                                toast("Error reading from DB");
                                //noinspection ThrowableResultOfMethodCallIgnored
                                Timber.e("Error getting filtered list: %s", task.getError().getMessage());
                                stopTimer();
                            } else {
                                filteredList.clear();
                                filteredList.addAll(task.getResult());
                                updateTitle();
                            }

                            return null;
                        }
                    }, Task.UI_THREAD_EXECUTOR);
        }
    }

    private void updateTitle() {
        String text;
        if (startedForResult()) {
            text = getString(R.string.passed_string) + ": " +
                    getArguments().getString(CompleteMeActivity.PASSED_WORD_KEY);
        } else {
            if (inputEditText.length() < SEARCH_LENGTH_THRESHOLD) {
                text = getString(R.string.type_2_chars);
            } else {
                if (filteredList.size() != 1) {
                    text = getString(R.string.found_words, filteredList.size());
                } else {
                    text = getString(R.string.found_one_word);
                }
            }
        }

        titleTextView.setText(text);
        listRecyclerView.getAdapter().notifyDataSetChanged();
    }

    private void startTimer() {
        timer = new Timer();
        initPeriodicTimerTask();
        timer.schedule(timerTask, TimeUnit.SECONDS.toMillis(PERIODIC_START_DELAY),
                TimeUnit.SECONDS.toMillis(PERIODIC_RATE));
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void initPeriodicTimerTask() {
        final Handler handler = new Handler();
        swipeRefreshLayout.setRefreshing(true);
        toast(getString(R.string.loading));
        timerTask = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        final DataAccessor dataAccessor = DataAccessor.getInstance(getActivity());
                        dataAccessor.updateDataBase().continueWith(new Continuation<Void, Void>() {

                            @Override
                            public Void then(Task<Void> task) throws Exception {
                                if (task.isFaulted()) {
                                    //noinspection ThrowableResultOfMethodCallIgnored
                                    Timber.e("Failed to update database: %s", task.getError().getMessage());
                                    toast(getString(R.string.file_not_found));
                                    stopTimer();
                                    swipeRefreshLayout.setRefreshing(false);
                                } else {
                                    if (!dataAccessor.beingUpdated()) {
                                        swipeRefreshLayout.setRefreshing(false);
                                    }
                                    if (startedForResult()) {
                                        getFilteredList(getArguments().getString(CompleteMeActivity.PASSED_WORD_KEY));
                                    } else {
                                        getFilteredList(inputEditText.getText().toString());
                                    }
                                }

                                return null;
                            }
                        }, Task.UI_THREAD_EXECUTOR);
                    }
                });
            }
        };
    }

    private void toast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private boolean startedForResult() {
        return getArguments() != null && getArguments().getString(CompleteMeActivity.PASSED_WORD_KEY) != null;
    }

    public interface ItemClickedListener {
        void onClickItem(String word);
    }

    public interface OnDataReceived {
        void onDataReceived(String selectedWord);
    }

    public static class WordsAdapter extends RecyclerView.Adapter<WordsAdapter.ViewHolder> {
        private final List<String> list;
        private final ItemClickedListener listener;

        private WordsAdapter(List<String> list, ItemClickedListener listener) {
            this.list = list;
            this.listener = listener;
        }

        @Override
        public WordsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_view,
                    parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(WordsAdapter.ViewHolder holder, final int position) {
            holder.cardTextView.setText(list.get(position));
            holder.view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    listener.onClickItem(list.get(position));
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {
            private final View view;

            @InjectView(R.id.card_text) TextView cardTextView;

            public ViewHolder(View view) {
                super(view);
                this.view = view;
                ButterKnife.inject(this, view);
            }
        }
    }

    private class InputWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text = s.toString();
            if (SEARCH_LENGTH_THRESHOLD > text.length()) {
                getFilteredList("");
            } else {
                getFilteredList(text);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
