package com.mark1492.completeme;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

// Shared preferences helper class
public class CompleteMePreferences {
    private static CompleteMePreferences instance = null;
    private final SharedPreferences preferences;

    private CompleteMePreferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static CompleteMePreferences getInstance(Context context) {
        if (instance == null) {
            instance = new CompleteMePreferences(context);
        }

        return instance;
    }

    private void setLong(String id, long value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(id, value);
        editor.apply();
    }

    private long getLong(String id) {
        return preferences.getLong(id, 0);
    }

    public void setFileModifiedTime(String fileName, long time) {
        setLong(fileName, time);
    }

    public long getFileModifiedTime(String fileName) {
        return getLong(fileName);
    }
}
