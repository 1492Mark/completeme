package com.mark1492.completeme;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Task;
import timber.log.Timber;

public class DataAccessor {
    private static final String SOURCE_FILE = "CompleteMeDic.txt";
    private static DataAccessor instance = null;
    private final Context context;
    private boolean dbUpdateIsRunning = false;

    private DataAccessor(Context context) {
        this.context = context;
    }

    public static DataAccessor getInstance(Context context) {
        if (instance == null) {
            instance = new DataAccessor(context);
        }

        return instance;
    }

    public Task<ArrayList<String>> getFilteredList(final String startsWithString) {
        final Task<Object>.TaskCompletionSource tcs = Task.create();

        Task.callInBackground(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                List<String> filteredList = SQLiteHelper.getInstance(context).getWordsBeginningWith(startsWithString);
                TSTSearchEngine<String> engine = new TSTSearchEngine<>(filteredList);
                tcs.setResult(engine.searchAll(startsWithString));
                return null;
            }
        });

        return tcs.getTask().cast();
    }

    public boolean beingUpdated() {
        return dbUpdateIsRunning;
    }

    public Task<Void> updateDataBase() {
        final Task<Object>.TaskCompletionSource tcs = Task.create();
        final DataStore dataStore = new FileDataStore(context, SOURCE_FILE);

        if (!dataStore.dataModified() || dbUpdateIsRunning) {
            tcs.setResult(null);
            Timber.w("data was not changed / DB update is already running");
        } else {
            dbUpdateIsRunning = true;
            Task.callInBackground(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    boolean success = true;
                    if (!dataStore.open()) {
                        tcs.setError(new RuntimeException("Failed to open data store: "
                                + dataStore.getDataStorePath()));
                        return null;
                    }

                    try {
                        while (true) {
                            ArrayList<String> currentBatch = dataStore.getNextResults();
                            if (currentBatch.size() > 0) {
                                SQLiteHelper.getInstance(context).addWords(currentBatch);
                            } else {
                                break;
                            }
                        }

                    } catch (Exception ex) {
                        success = false;
                        tcs.setError(ex);
                        return null;
                    } finally {
                        dataStore.close(success);
                        dbUpdateIsRunning = false;
                    }

                    // For debug
                    long sqlWriteTime = SQLiteHelper.getInstance(context).getTime();
                    long readTime = dataStore.getProcessingTime();
                    Timber.e("Total time: %ss, DB: %ss, Read: %ss", sqlWriteTime + readTime, sqlWriteTime, readTime);

                    tcs.setResult(null);
                    return null;
                }
            });
        }

        return tcs.getTask().cast();
    }
}
