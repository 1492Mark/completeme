package com.mark1492.completeme;

import java.util.ArrayList;

public interface DataStore {
    ArrayList<String> getNextResults();

    boolean dataModified();

    //For performance measuring
    long getProcessingTime();

    void close(boolean success);

    boolean open();

    String getDataStorePath();
}
