package com.mark1492.completeme;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class FileDataStore implements DataStore {
    public static final int READ_LINES_BUFFER = 100000;
    BufferedReader bufferedReader = null;
    private long time = 0;
    private Context context;
    private File file;

    public FileDataStore(Context context, String fileName) {
        this.context = context;
        this.file = new File(Environment.getExternalStorageDirectory().toString() + "/" + fileName);
    }

    @Override
    public ArrayList<String> getNextResults() {
        long startTime = new Date().getTime();

        String currentLine;
        ArrayList<String> list = new ArrayList<>();

        try {
            for (int i = 0; i < READ_LINES_BUFFER; i++) {
                if ((currentLine = bufferedReader.readLine()) != null) {
                    list.add(cleanString(currentLine));
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Error reading from file: %s" + e.getMessage());
        }

        time = time + (new Date().getTime() - startTime);
        return list;
    }

    private String cleanString(String string) {
        // Check string for spaces / illegal characters - future implementation
        return string;
    }

    @Override
    public long getProcessingTime() { //For debug
        return time / 1000;
    }

    @Override
    public void close(boolean success) {
        if (success) {
            CompleteMePreferences.getInstance(context).setFileModifiedTime(file.getName(), file.lastModified());
        }

        bufferedReader = null;
    }

    @Override
    public boolean open() {
        boolean success = true;

        if (bufferedReader == null) {
            try {
                bufferedReader = new BufferedReader(new FileReader(file));
            } catch (FileNotFoundException e) {
                success = false;
            }
        }

        return success;
    }

    @Override
    public String getDataStorePath() {
        return file.getPath();
    }

    @Override
    public boolean dataModified() {
        long lastUpdatedTime = CompleteMePreferences.getInstance(context).getFileModifiedTime(file.getName());
        return !file.exists() || lastUpdatedTime != file.lastModified();
    }
}
