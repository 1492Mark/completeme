package com.mark1492.completeme;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.Date;

public class SQLiteHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "CompleteMe.db";
    private static final int DATABASE_VERSION = 1;
    private static final String WORDS_TABLE_NAME = "words";
    private static final String WORD_COLUMN_NAME = "word";
    private static SQLiteHelper instance = null;
    private SQLiteDatabase db;
    private long time = 0;

    private SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    public static SQLiteHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SQLiteHelper(context);
        }

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + WORDS_TABLE_NAME + "(" + WORD_COLUMN_NAME + " TEXT PRIMARY KEY NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + WORDS_TABLE_NAME);
        onCreate(db);
    }

    //For debug
    public long getTime() {
        return time / 1000;
    }

    public void addWords(ArrayList<String> list) {
        long startTime = new Date().getTime();

        openDB();

        String sqlStatement = "INSERT OR IGNORE INTO " + WORDS_TABLE_NAME + " ("
                + WORD_COLUMN_NAME + ") VALUES (?);";

        db.beginTransaction();
        SQLiteStatement statement = db.compileStatement(sqlStatement);

        for (String word : list) {
            statement.bindString(1, word);
            statement.executeInsert();
            statement.clearBindings();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        closeDB();

        time = time + (new Date().getTime() - startTime);
    }

    public ArrayList<String> getWordsBeginningWith(String word) {
        ArrayList<String> list = new ArrayList<>();

        openDB();

        Cursor cursor = db.rawQuery("SELECT * FROM " + WORDS_TABLE_NAME + " WHERE " + WORD_COLUMN_NAME
                + " LIKE ?", new String[]{word + '%'});
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                list.add(cursor.getString(cursor.getColumnIndex(WORD_COLUMN_NAME)));
                cursor.moveToNext();
            }
        }

        cursor.close();
        closeDB();
        return list;
    }

    private void openDB() {
        if (db == null || !db.isOpen()) {
            db = getWritableDatabase();
        } else {
            db.acquireReference();
        }
    }

    private void closeDB() {
        if (db != null && db.isOpen()) {
            db.releaseReference();
        }
    }
}
