package com.mark1492.completeme;

import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the TSTSearchEngine logic.
 */
@SmallTest
public class TSTSearchEngineTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Test
    public void tstSearchEngine_InputNull() {
        List<String> inputList = null;
        List<String> expectedList = new ArrayList<>();
        String searchPrefix = "Aa";
        thrown.expect(NullPointerException.class);

        //noinspection ConstantConditions
        TSTSearchEngine<String> engine = new TSTSearchEngine<>(inputList);
        assertEquals(expectedList, engine.searchAll(searchPrefix));
    }

    @Test
    public void tstSearchEngine_InputEmpty() {
        List<String> inputList = new ArrayList<>();
        List<String> expectedList = new ArrayList<>();
        String searchPrefix = "Aa";

        TSTSearchEngine<String> engine = new TSTSearchEngine<>(inputList);
        assertEquals(expectedList, engine.searchAll(searchPrefix));
    }

    @Test
    public void tstSearchEngine_Alphabetical() {
        List<String> inputList = new ArrayList<>(Arrays.asList("aaa", "aab", "aac"));
        List<String> expectedList = new ArrayList<>(Arrays.asList("aaa", "aab", "aac"));
        String searchPrefix = "aa";

        TSTSearchEngine<String> engine = new TSTSearchEngine<>(inputList);
        assertEquals(expectedList, engine.searchAll(searchPrefix));
    }

    @Test
    public void tstSearchEngine_AlphabeticalReversed() {
        List<String> inputList = new ArrayList<>(Arrays.asList("aac", "aab", "aaa"));
        List<String> expectedList = new ArrayList<>(Arrays.asList("aaa", "aab", "aac"));
        String searchPrefix = "aa";

        TSTSearchEngine<String> engine = new TSTSearchEngine<>(inputList);
        assertEquals(expectedList, engine.searchAll(searchPrefix));
    }

    @Test
    public void tstSearchEngine_Duplicates() {
        List<String> inputList = new ArrayList<>(Arrays.asList("aaa", "aaa"));
        List<String> expectedList = new ArrayList<>(Collections.singletonList("aaa"));
        String searchPrefix = "aa";

        TSTSearchEngine<String> engine = new TSTSearchEngine<>(inputList);
        assertEquals(expectedList, engine.searchAll(searchPrefix));
    }

    @Test
    public void tstSearchEngine_Capital() {
        List<String> inputList = new ArrayList<>(Arrays.asList("Aaa", "aab", "Aac"));
        List<String> expectedList = new ArrayList<>(Collections.singletonList("aab"));
        String searchPrefix = "aa";

        TSTSearchEngine<String> engine = new TSTSearchEngine<>(inputList);
        assertEquals(expectedList, engine.searchAll(searchPrefix));
    }

    @Test
    public void tstSearchEngine_Caps() {
        List<String> inputList = new ArrayList<>(Arrays.asList("AAA", "AAB", "AAC"));
        List<String> expectedList = new ArrayList<>(Arrays.asList("AAA", "AAB", "AAC"));
        String searchPrefix = "AA";

        TSTSearchEngine<String> engine = new TSTSearchEngine<>(inputList);
        assertEquals(expectedList, engine.searchAll(searchPrefix));
    }

    @Test
    public void tstSearchEngine_EmptyString() {
        List<String> inputList = new ArrayList<>(Arrays.asList("AAA", "AAB", ""));
        List<String> expectedList = new ArrayList<>(Arrays.asList("AAA", "AAB", "AAC"));
        String searchPrefix = "AA";
        thrown.expect(IllegalArgumentException.class);

        TSTSearchEngine<String> engine = new TSTSearchEngine<>(inputList);
        assertEquals(expectedList, engine.searchAll(searchPrefix));
    }


    @Test
    public void tstSearchEngine_Mixed() {
        List<String> inputList = new ArrayList<>(Arrays.asList("AAZ", "AAW", "AAW", "AAW", "AAX",
                "aaZ", "aaA", "AAa", "AAb", "AAc", "AAB", "AAA", "AAW", "BBa", "BBZ", "zzz"));
        List<String> expectedList = new ArrayList<>(Arrays.asList("AAA", "AAa", "AAB", "AAb", "AAc",
                "AAW", "AAX", "AAZ"));
        String searchPrefix = "AA";

        TSTSearchEngine<String> engine = new TSTSearchEngine<>(inputList);
        assertEquals(expectedList, engine.searchAll(searchPrefix));
    }
}